# reaction-time

Custom CSS stylesheet - *Reaction Time Is A Factor In This* - for my [blog](https://www.dwarmstrong.org).

## AUTHOR

[Daniel Wayne Armstrong](https://www.dwarmstrong.org)

## LICENSE

GPLv3. See [LICENSE](https://gitlab.com/dwarmstrong/homebin/-/blob/master/LICENSE.md) for more details.
